# LedgerStrait #

LedgerStrait is an automated AI-enabled blockchain that adds IoT sensors, resiliency and visibility to the global supply chain.